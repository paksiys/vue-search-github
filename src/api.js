import axios from 'axios'

var defaultURL = "https://api.github.com/"
var rawURL = "https://raw.githubusercontent.com/"

//searching the username profile
const getUser = (response, username)=>{
  return axios.get(defaultURL + 'users/' + username)
      .then(response);
}

//searching the user repos
const getRepos = (response, username)=>{
  return axios.get(defaultURL + 'users/' + username + '/repos')
      .then(response);
}

//searching the user readme raw data
const getReadme = (response, username, repoName)=>{
  return axios.get(rawURL + username + "/" + repoName+ '/master/README.md')
      .then(response);
}


export {getUser, getRepos, getReadme};
