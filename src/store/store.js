import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state:{
    username:'',
    reponame:''
  },
  mutations:{
    setUser(state, username){
      state.username=username
    },
    setRepo(state, reponame){
      state.reponame=reponame
    }
  },
})
