# Vue Markdown Preview

In this web app, you will be able to search the profile and projects/repos from a github user. You could also see the Readme.md file directly.

## Installation

Before you get started, make sure you have a node js installed in your PC, then install the packages in this folder with

```bash
npm install
```
The used packages are:
* axios: ^0.16.1
* marked: ^0.7.0
* vue: 2.0.1
* vue-router: ^3.1.3
* vuex : ^3.1.1"


## Usage
For running the web app, type this command below
```npm
npm run dev
```

## Development
If you want to adding more features from github, find the details in
[Github Developer API](https://developer.github.com/v3/)

## Contact
If you have any question about this project, send me an email paksiyudhasasmita@gmail.com

## License
[MIT](https://choosealicense.com/licenses/mit/)
