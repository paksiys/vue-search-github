import Vue from 'vue'
import axios from 'axios'
import App from './App.vue'
import router from './router/router.js'
import { store } from './store/store.js'

window.axios = axios;

new Vue({
  el: '#app',
  router,
  store,
  render: (h) => h(App)
})
