import Vue from "vue";
import VueRouter from 'vue-router';
import Readme from '../components/Readme.vue';
import Search from '../components/Search.vue';
import NotFound from '../components/NotFound.vue';

Vue.use(VueRouter);

//path list for routing
const routes = [
  {path:'/', name:'search' , component:Search},
  {path:'/readme/:username/:reponame', name:'readme', component:Readme},
  {path:'*', name:'notfound' , component:NotFound},
];

export default new VueRouter({
  routes:routes,
  mode:'history',
});
